<?php

class Dinosaur extends Model
{
    public static $_table = 'Dinosaurs';

    public function model()
    {
        return $this->belongs_to('IngenModel', 'model_id')->find_one();
    }
}

class IngenModel extends model
{
    public static $_table = 'Models';

    public function specie()
    {
        return $this->belongs_to('Specie', 'specie_id')->find_one();
    }
}

class Specie extends Model
{
    public static $_table = 'Species';
}