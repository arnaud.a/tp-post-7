<?php

use Michelf\Markdown;

function renderHTMLFromMarkdown($string_markdown_formatted)
{
    return trim(Markdown::defaultTransform($string_markdown_formatted));
}

function readFileContent($filepath)
{
    if (file_exists($filepath)) {
        return file_get_contents($filepath);
    }
    throw new Exception("File doesn't exist!");
}