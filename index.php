<?php require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addFilter(new Twig_Filter('markdown', function ($string) {
        return renderHTMLFromMarkdown($string);
    }));
});

Flight::before('start', function (&$params, &$output) {
    ORM::configure('sqlite:ingen.sqlite3');
});

Flight::route('/', function () {
    $data = [
        'contenu' => 'Bowser',
    ];
    Flight::view()->display('index.twig', $data);
});

Flight::route('/dinosaurs', function () {
    $data = [
        'dinosaurs' => Dinosaur::find_many(),
    ];
    Flight::view()->display('dinosaurs.twig', $data);
});

Flight::route('/dinosaurs/@name', function ($name) {
    $data = [
        'dino' => Model::factory('Dinosaur')
            ->where('name', $name)
            ->find_one(),
    ];
    Flight::render('dinosaur.twig', $data);
});

Flight::route('/formulaire', function () {
    if (isset($_POST['dino_name'])) {
        $dino = Model::factory('Dinosaur')->create();
        $dino->name = $_POST['dino_name'];
        $dino->model_id = $_POST['dino_model_id'];
        $dino->birthday = $_POST['dino_birthday'];
        $dino->save();
    }
    Flight::view()->display('form.twig');
});

Flight::start();